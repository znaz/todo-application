import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./Routes/rootLayout";
import ErrorPage from "./errorPage";
import HomePage from "./Routes/homePage";
import AddTodo from "./Routes/addTodo";
import CompletedTodos from "./Routes/completedTodos";
import PendingTodos from "./Routes/pendingTodos";
import store from "./app/store";
import { Provider } from "react-redux";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/",
        element: <HomePage />,
      },
      {
        path: "/add-todo",
        element: <AddTodo />,
      },
      {
        path: "/completed-todos",
        element: <CompletedTodos />,
      },
      {
        path: "/pending-todos",
        element: <PendingTodos />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
