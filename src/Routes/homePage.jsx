import React from 'react'
import { Link } from 'react-router-dom'

function homePage() {
  return (
    <main className='flex flex-col justify-center items-center'>
        <div className='flex flex-col justify-center items-center gap-4'>
        <h1 className="font-bold text-5xl text-purple-900">Welcome</h1>
        <p className="font-semibold uppercase text-center">to the <br />ToDo Application</p>
        </div>
        <Link to={"/add-todo"} className='mt-8 font-semibold underline text-blue-600'>Start adding todos</Link>
    </main>
  )
}

export default homePage