import React from "react";
import { useSelector } from "react-redux";

function CompletedTodos() {
  const allTodos = useSelector((state) => state.todos);

  const completedTodos = allTodos.filter((todo) => todo.done);

  return (
    <main className="w-4/5 mx-auto">
      <div className="flex flex-col  mt-12 gap-6  ">
        <h1 className="text-2xl font-semibold ml-24">Completed Todos</h1>
        <ul className="flex flex-col justify-center gap-6 mx-24">
          {completedTodos.map((todo, index) => (
            <li
              className={`shadow-lg p-6 flex justify-between items-center bg-green-400`}
              key={index}
            >
              <h2 className="text-lg font-semibold ">{todo.text}</h2>
            </li>
          ))}
        </ul>
      </div>
    </main>
  );
}

export default CompletedTodos;
