import React from "react";
import { useSelector } from "react-redux";

function PendingTodos() {
  const allTodos = useSelector((state) => state.todos);

  const pendingTodos = allTodos.filter((todo) => !todo.done);

  return (
    <main className="flex flex-col mt-12 gap-6 w-4/5 mx-auto">
      <h1 className="text-2xl font-semibold ml-24">Pending Todos</h1>
      <ul className="flex flex-col justify-center gap-6 mx-24">
        {pendingTodos.map((todo, index) => (
          <li
            className={`shadow-lg p-6 flex justify-between items-center bg-slate-200 `}
            key={index}
          >
            <h2 className="text-lg font-semibold ">{todo.text}</h2>
          </li>
        ))}
      </ul>
    </main>
  );
}

export default PendingTodos;
