import React from "react";
import { useSelector } from "react-redux";
import Todos from "../components/Todos";
import TodosForm from "../components/TodosForm";

function AddTodo() {
  const todosList = useSelector((state) => state.todos);

  return (
    <main className="flex flex-col gap-6 ">
      <div className="w-2/5 flex flex-col justify-center items-center shadow-lg gap-6 p-4 mx-auto mt-24">
        <h1 className="font-semibold text-xl uppercase ">Add new Todo</h1>
        <TodosForm />
      </div>
      <div className="mx-auto w-4/5">
        <ul className="flex flex-col-reverse justify-center gap-6 mx-24">
          {todosList.map((todo, index) => {
            return <Todos key={index} todo={todo} index={index} />;
          })}
        </ul>
      </div>
    </main>
  );
}

export default AddTodo;
