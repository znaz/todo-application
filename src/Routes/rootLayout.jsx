import React from "react";
import { Link, Outlet } from "react-router-dom";

function rootLayout() {
  return (
    <>
      <header className="bg-purple-800 h-16 flex flex-row justify-between items-center px-4">
        <h1 className="font-bold text-xl">Todo App</h1>
        <nav>
          <ul className="text-white font-semibold flex flex-row gap-4">
            <Link className="hover:text-orange-500 hover:underline" to={"/"}>Home</Link>
            <Link className="hover:text-orange-500 hover:underline" to={"/add-todo"}>Todos</Link>
            <Link className="hover:text-orange-500 hover:underline" to={"completed-todos"}>Completed</Link>
            <Link className="hover:text-orange-500 hover:underline" to={"/pending-todos"}>Pending</Link>
          </ul>
        </nav>
      </header>
      <main>
        <Outlet />
      </main>
      <footer className="h-12 bg-slate-800 text-white flex flex-row justify-between items-center px-4">
        <div className="flex gap-2">
          <span>Copyright &copy; 2024</span>
          <span>Todo APP</span>
        </div>
        <span>Developed by Shinas</span>
      </footer>
    </>
  );
}

export default rootLayout;
