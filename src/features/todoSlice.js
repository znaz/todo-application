import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
  name: "todos",
  initialState: {
    todos: JSON.parse(localStorage.getItem("todos")) || [""],
  },
  reducers: {
    addTodo: (state, action) => {
      state.todos.push(action.payload);
      localStorage.setItem("todos", JSON.stringify(state.todos));
    },
    deleteTodo: (state, action) => {
      const index = action.payload;
      state.todos.splice(index, 1); 
      localStorage.setItem("todos", JSON.stringify(state.todos));
    },
    doneTodo: (state, action) => {
      const index = action.payload;
      state.todos[index].done =!state.todos[index].done;
      localStorage.setItem("todos", JSON.stringify(state.todos));
    },
  },
});

export const { addTodo, deleteTodo,doneTodo } = counterSlice.actions;

export default counterSlice.reducer;
