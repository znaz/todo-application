import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../features/todoSlice";

function TodosForm() {
  const [todos, setTodos] = useState("");

  const dispatch = useDispatch();
  const handleChange = (e) => {
    setTodos(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (todos.trim() === "") {
      return;
    }
    dispatch(
      addTodo({
        text: todos,
        done: false,
      })
    );

    setTodos("");
  };

  return (
    <form className="flex flex-col items-center w-3/5" onSubmit={handleSubmit}>
      <input
        className="bg-purple-100 w-full p-2"
        type="text"
        value={todos}
        onChange={handleChange}
      />
      <button
        type="submit"
        className="bg-purple-800 px-4 rounded-sm font-semibold text-white mt-4"
      >
        Add
      </button>
    </form>
  );
}

export default TodosForm;
