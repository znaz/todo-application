import React from "react";
import { useDispatch } from "react-redux";
import { deleteTodo, doneTodo } from "../features/todoSlice";

function Todos({ todo, index }) {
  const dispatch = useDispatch();

  const handleDelete = (index) => {
    dispatch(deleteTodo(index));
  };

  const handleDone = (index) => {
    dispatch(doneTodo(index));
  };
  return (
    <li
      className={`shadow-lg p-6 flex justify-between items-center ${
        todo.done ? "bg-green-400" : ""
      }`}
    >
      <h2 className="text-lg font-semibold ">{todo.text}</h2>
      <div className="flex gap-6">
        <button
          className="bg-red-700 text-white px-2 rounded"
          onClick={() => handleDelete(index)}
        >
          Delete
        </button>{" "}
        <button
          className="bg-green-600 text-white px-2 rounded"
          onClick={() => handleDone(index)}
        >
          {todo.done ? "Undone" : "Done"}
        </button>
      </div>
    </li>
  );
}

export default Todos;
